<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="confirm.css">
</head>

<body>
    <div class="main">
        <div class="search-form">
            <form action="" method="get" id="search-form" onsubmit="return false">
                <div class="form-group">
                    <label class="form-label">Phân khoa</label>
                    <div class="form-input">
                        <select name="falcuty" id="falcuties">
                            <option value="111" class="falcuty-option"></option>
                            <option value="MAT" class="falcuty-option">Khoa học máy tính</option>
                            <option value="KDL" class="falcuty-option">Khoa học dữ liệu</option>

                        </select>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Từ khóa</label>
                    <div class="form-input">
                        <input type="text" name="keyword" class="keyword" value=''>
                    </div>

                </div>
                <div class="form-btn">
                    <div class="delete-input btn">Xóa</div>
                    <input type="submit" class="btn" value="Tìm kiếm"></input>
                </div>
            </form>
        </div>
        <div class="count">
            <div class="student-found">
                <p>Số sinh viên tìm thấy: XXX</p>
            </div>
            <div class="btn">
                <a href="form.php" class="addBtn">Thêm</a>
            </div>
        </div>
        <div class="student-list">
            <table class="student-table">
                <tr class="header">
                    <td style="width:20%">No.</td>
                    <td style="width:50%">Tên sinh viên</td>
                    <td style="width:30%">Khoa</td>
                    <td style="text-align:center;width:10%">Action</td>
                </tr>
                <tr class="student-item">
                    <td>1</td>
                    <td style="">Nguyen Văn Aa</td>
                    <td>Khoa học máy tính</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>2</td>
                    <td style="">Mạc Văn Ba</td>
                    <td>Khoa học máy tính</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>3</td>
                    <td style="">Mạc Văn Ca</td>
                    <td>Khoa học dữ liệu</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>4</td>
                    <td style="">Mạc Văn Da</td>
                    <td>Khoa học dữ liệu</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script>
        var deleteInputBtn = document.querySelector('.delete-input');
        var falcutyInput = document.querySelector('#falcuties');
        var keywordInput = document.querySelector('.keyword');
        var submitBtn = document.querySelector('input.btn');
        var falcutyIndex = '';
        var keyword = '';
        var form = document.querySelector('form');

        deleteInputBtn.onclick = function() {
            console.log(falcutyInput, keywordInput);
            // keywordInput.value = ''
            document.getElementById('search-form').reset();
        }
    </script>
</body>

</html>
